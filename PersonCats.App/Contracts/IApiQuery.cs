﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonCats.App.Contracts
{
    public interface IApiQuery
    {
        string Uri { get; set; }
        string Query { get; }
    }
}
