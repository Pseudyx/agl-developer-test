﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PersonCats.App.Contracts
{
    public interface IJsonAPIClient : IDisposable
    {
        Task<T> GetAsync<T>(string query = null);
        T Get<T>(string query = null);
        HttpClient Client { get; }
    }
}
