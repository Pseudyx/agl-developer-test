﻿using System;
using System.Linq;

namespace PersonCats.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var json = JsonAPIFacade.GetPeople();

            Console.WriteLine("== Male ==");
            json.Where(p => p.Gender.ToUpper() == "MALE" && p.Pets != null)
                .SelectMany(x => x.Pets.Where(y => y.Type.ToUpper() == "CAT"))
                .OrderBy(p => p.Name).ToList()
                .ForEach(x => Console.WriteLine(x.Name));
            Console.WriteLine();

            Console.WriteLine("== Female ==");
            json.Where(p => p.Gender.ToUpper() == "FEMALE" && p.Pets != null)
                .SelectMany(x => x.Pets.Where(y => y.Type.ToUpper() == "CAT"))
                .OrderBy(p => p.Name).ToList()
                .ForEach(x => Console.WriteLine(x.Name));
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
