﻿using PersonCats.App.Domain;
using PersonCats.App.Service;
using System.Collections.Generic;

namespace PersonCats.App
{
    public static class JsonAPIFacade
    {
        public static JsonAPIClient GetClient()
        {
            return new JsonAPIClient("http://agl-developer-test.azurewebsites.net");
        }

        private static T Get<T>(string query)
        {
            using (var client = GetClient())
            {
                return client.Get<T>(query);
            }
        }

        public static List<Person> GetPeople()
        {
            return Get<List<Person>>("people.json");
        }
    }
}
