﻿using PersonCats.App.Contracts;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PersonCats.App.Service
{
    public class JsonAPIClient : IJsonAPIClient
    {
        public HttpClient Client { get; private set; }

        public JsonAPIClient(string uri)
        {
            Client = new HttpClient { BaseAddress = new Uri(uri) };
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<T> GetAsync<T>(string query = null)
        {
            var response = await Client.GetAsync(query);
            return await response.Content.ReadAsAsync<T>();
        }

        public T Get<T>(string query = null)
        {
            var response = Client.GetAsync(query).Result;
            return response.Content.ReadAsAsync<T>().Result;
        }

        public void Dispose()
        {
            Client.Dispose();
        }
    }
}
