﻿using NUnit.Framework;
using PersonCats.App;
using PersonCats.App.Domain;
using System.Linq;

namespace PersonCats.Test
{
    [TestFixture]
    public class PersonCatsTest
    {
        [Test]
        public void QueryJsonDataReturnsValue()
        {
            var json = JsonAPIFacade.GetPeople();
            Assert.IsNotNull(json);
        }

        [Test]
        public void ReturnedValueIsDeserialisedIntoDataDomain()
        {
            var json = JsonAPIFacade.GetPeople();
            var person = json.First();
            Assert.AreSame(typeof(Person), person.GetType());
        }
    }
}
